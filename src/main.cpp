#include <euler_method_impl/solver.hpp>
#include <euler_method_impl/semi_implicit_solver.hpp>
#include <euler_method_impl/utils.hpp>

#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    const std::string expression_string =
                    "clamp(-1.0,sin(2 * pi * x) + cos(x / 2 * pi),+1.0)";
    
    const std::string expr1 = "6 * x^2 + 5 * x * y";
    // const std::string expr2 = "2 * x^3 + 3 * x * y";
    // const std::string expr3 = "6 * x^3 + 2 * x^2";
    // std::cout << integrate(expr3, 5, 8, 10000);
    // SemiImplicitSolver solver(expr1, expr2, 1, 1, 1);
    // solver.Solve();
    // solver.PlotSolution(true);

    Solver solver("-2 * y + 2 - exp(-4 * x)", "1 + 0.5 * exp(-4 * x) - 0.5 * exp(-2 * x)", 0, 1);
    // solver.UseDifferentStepSize(0.7, 1, 0.3, 0, 5, false);
    solver.BasicMethod();
    Values abm = solver.MidpointMethod();
    // Values bm = solver.AdamsBashforth();
    // for (int i = 0; i < 101; ++i) {
    //     std::cout << '(' << bm.x[i] << ", " << bm.y[i] << ')' << " " << '(' << abm.x[i] << ", " << abm.y[i] << ")\n";
    // }
    // solver.PlotSolution(false, SolutionType::kAdamsBashforth);
    // solver.PlotCompareWithExactSolution(false, SolutionType::kAdamsBashforth);

    // solver.PlotSolution(true);
    // solver.PlotCompareWithExactSolution(true, SolutionType::kMidpoint);
    solver.PlotCompareAll(false);
    solver.PrintErrors(0.1, 20);
    return 0;
}