project(data_feeder)
cmake_minimum_required(VERSION 3.8)

set(CMAKE_CXX_STANDARD 17)

set(CMAKE_CXX_FLAGS_ASAN "-g -fsanitize=address,undefined -fno-sanitize-recover=all"
    CACHE STRING "Compiler flags in asan build"
    FORCE)

set(CMAKE_CXX_FLAGS_TSAN "-g -fsanitize=thread -fno-sanitize-recover=all"
    CACHE STRING "Compiler flags in tsan build"
    FORCE)

find_package(Python2 REQUIRED COMPONENTS Development NumPy)

set(ALL_INCLUDES
    include
    contrib/matplotlib/include
    contrib/exprtk/include
    )

add_executable(main "src/main.cpp")
target_include_directories(main PUBLIC ${ALL_INCLUDES} ${Python2_INCLUDE_DIRS} ${Python2_NumPy_INCLUDE_DIRS})
target_link_libraries(main Python2::Python Python2::NumPy)