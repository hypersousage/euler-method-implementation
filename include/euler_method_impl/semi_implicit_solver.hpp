#pragma once

#include <iostream>
#include <string>
#include <optional>
#include <vector>
#include <unordered_map>

#include <matplotlib/matplotlibcpp.hpp>
#include <euler_method_impl/expression.hpp>

class SemiImplicitSolver {
    public:
        explicit SemiImplicitSolver(
            const std::string &expr_v, const std::string &expr_x, double x_0, double v_0, double t_0, double time_step=0.01, int64_t n_iters=100
        ) : expr_v_(expr_v), expr_x_(expr_x),  x_0_(x_0), v_0_(v_0), t_0_(t_0), time_step_(time_step), n_iters_(n_iters) {}

        void Solve() {
            Values solution_v(n_iters_ + 1);
            Values solution_x(n_iters_ + 1);
            
            expr_x_.x = t_0_;
            expr_x_.y = x_0_;
            expr_v_.x = t_0_;
            expr_v_.y = v_0_;
            solution_v.push_back(expr_v_.x, expr_v_.y);
            solution_x.push_back(expr_x_.x, expr_x_.y);
            for (int64_t i = 0; i < n_iters_; ++i) {
                expr_v_.y = expr_v_.y + expr_x_.value() * time_step_;
                solution_v.push_back(expr_v_.x + time_step_, expr_v_.y);

                expr_x_.y = expr_x_.y + expr_v_.value() * time_step_;
                solution_x.push_back(expr_x_.x + time_step_, expr_x_.y);
                expr_v_.x += time_step_;
                expr_x_.x += time_step_;
            }
            solution_v_ = solution_v;
            solution_x_ = solution_x;
        }

        void PlotSolution(bool save=false) {
            plt::figure_size(1200, 780);
            plt::named_plot("Computed solution for (dx/dt) = " + expr_v_.str_expr, solution_v_.x, solution_v_.y, ".-");
            plt::named_plot("Computed solution for (dv/dt) = " + expr_x_.str_expr, solution_x_.x, solution_x_.y, ".-");
            plt::legend();
            plt::title("Computed Solutions");
            if (save) {
                std::string path = "../plots/semi_implicit_method.png";
                plt::save(path);
            } else {
                plt::show();
            }
        }

    private:
        Expression expr_v_;
        Expression expr_x_;
        Expression exact_sol_expr_;
        double x_0_, v_0_, t_0_;
        double time_step_;
        int64_t n_iters_;
        Values solution_v_;
        Values solution_x_;

    
};