#pragma once

#include <exprtk/exprtk.hpp>

using SymbolTable      = exprtk::symbol_table<double>;
using ExprtkExpression = exprtk::expression<double>;
using Parser           = exprtk::parser<double>;

struct Values {
    std::vector<double> x, y;
    
    Values() {}

    Values(size_t size) {
        x.reserve(size);
        y.reserve(size);
    }

    void push_back(double x_value, double y_value) {
        x.push_back(x_value);
        y.push_back(y_value);
    }
};

struct Expression {
    std::string str_expr;
    ExprtkExpression expr;
    double x, y;
    Values values;

    Expression() {};
    
    Expression(const std::string &str_expr) : str_expr(str_expr) {
        SymbolTable symbol_table;
        symbol_table.add_variable("x", x);
        symbol_table.add_variable("y", y);
        symbol_table.add_constants();

        expr.register_symbol_table(symbol_table);

        Parser parser;
        parser.compile(str_expr, expr);
    }

    Values get_values(double x_start_point, double h, int64_t n_iters) {
        Values values(n_iters + 1);
        x = x_start_point;
        for (int i = 0; i < n_iters + 1; ++i) {
            values.y.push_back(expr.value());
            values.x.push_back(x);
            x += h;
        }
        return values;
    }

    double value() {
        return expr.value();
    }
};