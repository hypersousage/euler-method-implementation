#pragma once

#include <euler_method_impl/expression.hpp>

#include <string>
#include <iostream>

inline double integrate(const std::string &str_expr, double a, double b, double n_iters) {
    Expression expr(str_expr);
    double h = (b - a) / n_iters;
    expr.x = a;
    expr.y = 0;
    Values solution(n_iters + 1);
    for (int64_t i = 0; i < n_iters; ++i) {
        expr.y += h * expr.value();
        expr.x += h;
        std::cout << expr.x << ' ' << expr.y << '\n';
    }
    return expr.y;
}