#pragma once

#include <iostream>
#include <string>
#include <optional>
#include <vector>
#include <unordered_map>
#include <cmath>
#include <iomanip>

#include <matplotlib/matplotlibcpp.hpp>
#include <euler_method_impl/expression.hpp>

namespace plt = matplotlibcpp;

enum class SolutionType {
    kBasic,
    kAdamsBashforth,
    kMidpoint
};

std::unordered_map<SolutionType, std::string> sol_type_to_str = {
    {SolutionType::kBasic, "Basic Method"},
    {SolutionType::kMidpoint, "Midpoint Method"},
    {SolutionType::kAdamsBashforth, "Adams-Bashforth Method"},
};

class Solver {
    public:
        explicit Solver(
            const std::string &expr, double x_0, double y_0, double h=0.01, int64_t n_iters=100
        ) : expr_(expr), x_0_(x_0), y_0_(y_0), h_(h), n_iters_(n_iters) {}

        explicit Solver(
            const std::string &expr, const std::string &sol_expr, double x_0, double y_0,
            double h=0.01, int64_t n_iters=100
        ) : expr_(expr), exact_sol_expr_(sol_expr), x_0_(x_0), y_0_(y_0), h_(h), n_iters_(n_iters) {}

        Values BasicMethod() {
            expr_.x = x_0_;
            expr_.y = y_0_;
            Values solution(n_iters_ + 1);
            solution.x.push_back(expr_.x);
            solution.y.push_back(expr_.y);
            for (int64_t i = 0; i < n_iters_; ++i) {
                expr_.y += h_ * expr_.value();
                expr_.x += h_;
                solution.x.push_back(expr_.x);
                solution.y.push_back(expr_.y);
            }
            solutions_[SolutionType::kBasic] = solution;
            return solution;
        }

        Values MidpointMethod() {
            expr_.x = x_0_;
            expr_.y = y_0_;
            Values solution(n_iters_ + 1);
            solution.x.push_back(expr_.x);
            solution.y.push_back(expr_.y);
            for (int64_t i = 0; i < n_iters_; ++i) {
                double x = expr_.x;
                double y = expr_.y;
                expr_.y += 0.5 * h_ * expr_.value();
                expr_.x += 0.5 * h_;
                expr_.y = y + h_ * expr_.value();
                expr_.x = x + h_;
                solution.x.push_back(expr_.x);
                solution.y.push_back(expr_.y);
            }
            solutions_[SolutionType::kMidpoint] = solution;
            return solution;
        }

        Values AdamsBashforthMethod() {
            Values solution(n_iters_ + 1);
            expr_.x = x_0_;
            expr_.y = y_0_;
            solution.push_back(expr_.x, expr_.y);
            double prev_value = expr_.value();
            expr_.y += h_ * expr_.value();
            expr_.x += h_;
            solution.push_back(expr_.x, expr_.y);
            for (int i = 1; i < n_iters_; ++i) {
                double cur_value = expr_.value();
                expr_.y = expr_.y + 1.5 * h_ * expr_.value() - 0.5 * h_ *  prev_value;
                expr_.x += h_;
                prev_value = cur_value;
                solution.push_back(expr_.x, expr_.y);
            }
            solutions_[SolutionType::kAdamsBashforth] = solution;
            return solution;
        }

        void UseDifferentStepSize(double from, double to, double delta, double a, double b, bool save=false, SolutionType type=SolutionType::kBasic) {
            plt::figure_size(1200, 780);
            double old_h = h_;
            double old_n_iters = n_iters_;
            for (h_ = from; h_ <= to; h_ += delta) {
                n_iters_ = (b - a) / h_;
                Values solution = type == SolutionType::kBasic ? BasicMethod() : MidpointMethod();
                plt::named_plot("h = " + std::to_string(h_), solution.x, solution.y, ".-");
            }
            n_iters_ = (b - a) / to;
            Values exact_solution = exact_sol_expr_.get_values(x_0_, to, n_iters_);
            plt::named_plot("Exact solution: " + exact_sol_expr_.str_expr, exact_solution.x, exact_solution.y, ".-");
            plt::legend();
            plt::title("Exact Solution and Computed Solutions for (dy/dx) = " + expr_.str_expr);
            if (save) {
                std::string path = "../plots/using_different_step_size.png";
                plt::save(path);
            } else {
                plt::show();
            }
            h_ = old_h;
            n_iters_ = old_n_iters;
        }

        void PlotSolution(bool save=false, SolutionType type=SolutionType::kBasic) {
            Values solution = solutions_[type];
            plt::figure_size(1200, 780);
            plt::plot(solution.x, solution.y, ".-");
            plt::title("Computed Solution. " + sol_type_to_str[type]);
            if (save) {
                std::string path = "../plots/" + sol_type_to_str[type] + ".png";
                plt::save(path);
            } else {
                plt::show();
            }
        }

        void PlotCompareWithExactSolution(bool save=false, SolutionType type=SolutionType::kBasic) {
            Values solution = solutions_[type];
            Values exact_solution = exact_sol_expr_.get_values(x_0_, h_, n_iters_);
            plt::figure_size(1200, 780);
            plt::named_plot("Computed solution using " + sol_type_to_str[type], solution.x, solution.y, ".-");
            plt::named_plot("Exact solution: " + exact_sol_expr_.str_expr, exact_solution.x, exact_solution.y, ".-");
            plt::legend();
            plt::title("Exact Solution and Computed Solution for (dy/dx) = " + expr_.str_expr);

            if (save) {
                std::string path = "../plots/exact_and_" + sol_type_to_str[type] + ".png";
                plt::save(path);
            } else {
                plt::show();
            }
        }

        std::vector<std::pair<double, double>> GetErrors(const Values& sol, const Values &exact_sol) {
            std::vector<std::pair<double, double>> errors;
            errors.reserve(sol.x.size());
            for (int64_t i = 0; i < sol.x.size(); i++) {
                double cur_abs_error = std::fabs(sol.y[i] - exact_sol.y[i]);
                double cur_rel_error = std::round(cur_abs_error / exact_sol.y[i] * 100 * 1000) / 1000; 
                errors.emplace_back(cur_abs_error, cur_rel_error);
            }
            return errors;
        }

        void PrintErrors(double h, double n_iters, SolutionType type=SolutionType::kBasic) {
            double old_h = h_;
            double old_n_iters = n_iters_;
            h_ = h;
            n_iters_ = n_iters;
            Values solution;

            if (type == SolutionType::kBasic) {
                solution = BasicMethod();
            } else if (type == SolutionType::kMidpoint) {
                solution = MidpointMethod();
            } else if (type == SolutionType::kAdamsBashforth) {
                solution = AdamsBashforthMethod();
            }
            Values exact_sol = exact_sol_expr_.get_values(x_0_, h_, n_iters_);

            std::vector<std::pair<double, double>> errors = GetErrors(solution, exact_sol);

            const char separator    = ' ';
            const int nameWidth     = 16;

            std::cout << std::left << std::setw(nameWidth) << std::setfill(separator) << "iteration";
            std::cout << std::left << std::setw(nameWidth) << std::setfill(separator) << "abs error";
            std::cout << std::left << std::setw(nameWidth) << std::setfill(separator) << "relative error";
            std::cout << std::endl;

            int64_t it = 0;
            for (auto [abs_err, rel_err] : errors) {
                std::cout << std::left << std::setw(nameWidth) << std::setfill(separator) << it++;
                std::cout << std::left << std::setw(nameWidth) << std::setfill(separator) << abs_err;
                std::cout << std::left << std::setw(nameWidth) << std::setfill(separator) << std::to_string(rel_err) + "%";
                std::cout << std::endl;
            }

            n_iters_ = n_iters;
            h_ = old_h;
        }

        void PlotCompareAll(bool save=false) {
            Values exact_solution = exact_sol_expr_.get_values(x_0_, h_, n_iters_);
            plt::figure_size(1200, 780);
            for (auto [type, solution] : solutions_) {
                plt::named_plot("Computed solution using " + sol_type_to_str[type], solution.x, solution.y, ".-");
            }
            plt::named_plot("Exact solution: " + exact_sol_expr_.str_expr, exact_solution.x, exact_solution.y, ".-");
            plt::legend();
            plt::title("Exact Solution and Computed Solutions for (dy/dx) = " + expr_.str_expr);
            if (save) {
                std::string path = "../plots/compare_all.png";
                plt::save(path);
            } else {
                plt::show();
            }
        }

        void SetExactSolution(const std::string &expr) {
            exact_sol_expr_ = Expression(expr);
        }
    
    private:
        Expression expr_;
        Expression exact_sol_expr_;
        double x_0_, y_0_;
        double h_;
        int64_t n_iters_;
        std::unordered_map<SolutionType, Values> solutions_;
};
